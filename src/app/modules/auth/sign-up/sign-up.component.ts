import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
    selector     : 'auth-sign-up',
    templateUrl  : './sign-up.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignUpComponent implements OnInit
{
    @ViewChild('signUpNgForm') signUpNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signUpForm: FormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signUpForm = this._formBuilder.group({
                first_name: ['', [Validators.required,Validators.maxLength(7)]],
                last_name : ['',[Validators.required,Validators.maxLength(6)]],
                email     : ['',[Validators.required,Validators.email]],
                username  : [],
                password  : ['', [Validators.required, Validators.pattern('(?=.*[A-Z])(?=.*[!@#$&*%])(?=.*[0-9])(?=.*[a-z]).{8,}')]],
                //company   : [''],
                //agreements: ['', Validators.requiredTrue]
            }
        );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Sign up
     */
    signUp(): void
    {
        // Do nothing if the form is invalid
        
        
        if ( this.signUpForm.invalid )
        {
            return;
        }

        // Disable the form
        this.signUpForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign up
        this._authService.signUp(this.signUpForm.value)
            .subscribe(
                (response) => {

                    // Navigate to the confirmation required page
                    this._router.navigateByUrl('/confirmation-required');
                },
                (response) => {

                    // Re-enable the form
                    this.signUpForm.enable();

                    // Reset the form
                    this.signUpNgForm.resetForm();

                    // Set the alert
                    this.alert = {
                        type   : 'error',
                        message: 'Something went wrong, please try again.'
                    };

                    // Show the alert
                    this.showAlert = true;
                }
            );
    }
    genId():any{
        const firstName = this.signUpForm.get('first_name').value;
        const lastName = this.signUpForm.get('last_name').value;
        const username = this.signUpForm.get('username').value;
        console.log(firstName);
        var collection = ["#","$","%","&"];
        const symbol= collection[Math.floor(Math.random() * collection.length)];

        const num=Math.floor(Math.random() * 100) + 1;
        

        const alias = (firstName + num + symbol + lastName).toLowerCase();
        console.log((firstName +'#'+ lastName).toLowerCase());
        console.log(alias)
        this.signUpForm.patchValue({username:(alias).toLowerCase()});
        console.log(this.signUpForm.get('username').value)

        //this.signUpForm.patchValue({email:(alias+'@gmail.com').toLowerCase()});

    }
    

}
