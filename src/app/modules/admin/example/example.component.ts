import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
    selector     : 'example',
    templateUrl  : './example.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ExampleComponent implements OnInit
{
    /**
     * Constructor
     */
    username:any;
    id:any;
    public posts:any;
    createPostForm:FormGroup;
    constructor(private http: HttpClient,private _router: Router,private formBuilder:FormBuilder)
    {
        
    }
    ngOnInit(): void {

        this.createPostForm = this.formBuilder.group({
            userId          : [localStorage.getItem('username')],
            id              : [],
            title           : ['', ],
            body            : ['',]
        });


        this.loadPosts();

    }




    editPost(id:any):void
    {
        this._router.navigate(['/editPost',id]);
    }




    createPost():void{

        var _title=this.createPostForm.get('title').value;
        var _body=this.createPostForm.get('body').value;
        var _userId=this.createPostForm.get('userId').value;
        

        fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: _title ,
            body: _body,
            userId: _userId,
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8',
        },
        })
        .then((response) => response.json())
        .then((json) => {console.log(json)
            this.posts.push(json)})
        
        

        
        this.savePosts();
        this._router.navigateByUrl(this._router.url);

        
    }



    savePosts(){
        localStorage.setItem('posts',JSON.stringify(this.posts));
    }


    
    loadPosts(){
        const postInStorage=JSON.parse(localStorage.getItem('posts'));

        this.posts=JSON.parse(localStorage.getItem('posts'));
        
        if(!postInStorage){
        this.http.get('https://jsonplaceholder.typicode.com/posts').subscribe(
        (res) => {
          console.log(res);
        this.posts=res;

        })
        }

        this.id=this.posts.length+1;
        this.username=localStorage.getItem('username');
        this.savePosts();
        console.log('executed')
              
             
        
        
        

    }

}
