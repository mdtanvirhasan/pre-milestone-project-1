import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ExampleComponent } from 'app/modules/admin/example/example.component';
import {MatCardModule} from '@angular/material/card';
import { Component, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { UserComponent } from 'app/layout/common/user/user.component';
import { SharedModule } from 'app/shared/shared.module';
//import { profile } from 'console';
import { MatSelectModule } from '@angular/material/select';
import { ExampleGuestComponent } from './exampleGuest/example-guest/example-guest.component';



const exampleRoutes: Route[] = [
    {
        path     : '',
        component: ExampleComponent
    }
];

@NgModule({
    declarations: [
        ExampleComponent,
        ExampleGuestComponent
        
    ],
    imports     : [
        RouterModule.forChild(exampleRoutes),
        MatCardModule,
        MatDividerModule,
        MatInputModule,
        CommonModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatDividerModule,
        MatIconModule,
        MatMenuModule,
        SharedModule,
        ReactiveFormsModule,
        MatSelectModule,
        ReactiveFormsModule,FormsModule
    ]
})
export class ExampleModule
{
}
