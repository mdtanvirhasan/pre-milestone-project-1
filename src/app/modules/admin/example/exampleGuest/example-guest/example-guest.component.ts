import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-example-guest',
  templateUrl: './example-guest.component.html',
  styleUrls: ['./example-guest.component.scss']
})
export class ExampleGuestComponent implements OnInit {

  constructor(private http: HttpClient,private _router: Router) { }


  posts:any;
  username:any;
  ngOnInit(): void {

    this.http.get('https://jsonplaceholder.typicode.com/posts').subscribe(
            (res) => {
              console.log(res);
              this.posts=res;
              this.username=localStorage.getItem('username');
             
            })
  }

}
