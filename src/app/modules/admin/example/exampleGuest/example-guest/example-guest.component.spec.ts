import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExampleGuestComponent } from './example-guest.component';

describe('ExampleGuestComponent', () => {
  let component: ExampleGuestComponent;
  let fixture: ComponentFixture<ExampleGuestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExampleGuestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExampleGuestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
