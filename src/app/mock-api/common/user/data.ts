/* eslint-disable */
export const user = {
    id    : localStorage.getItem('username'),
    name  : localStorage.getItem('first_name'),
    email : localStorage.getItem('email'),
    avatar: localStorage.getItem('avatar'),
    status: 'online'
};
