import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { UserComponent } from 'app/layout/common/user/user.component';
import { SharedModule } from 'app/shared/shared.module';
//import { profile } from 'console';
import { EditPostComponent } from './edit-post.component';
import { MatSelectModule } from '@angular/material/select';
FormsModule

@NgModule({
    declarations: [
        EditPostComponent
    ],
    imports     : [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatDividerModule,
        MatIconModule,
        MatMenuModule,
        SharedModule,
        ReactiveFormsModule,
        MatSelectModule,
        ReactiveFormsModule,FormsModule
    ],
    exports     : [
        EditPostComponent,
        ReactiveFormsModule
    ]
})
export class EditPostModule
{

}