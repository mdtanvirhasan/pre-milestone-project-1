import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';
@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {

  postId:any;

  editPostForm:FormGroup;
  constructor(private route: ActivatedRoute,private http: HttpClient,private _authService: AuthService,private _formBuilder: FormBuilder,private _router: Router ) 
     { }

     

  ngOnInit(): void {

    this.editPostForm = this._formBuilder.group({
      title           : ['', ],
      description     : ['',]
    });
  }

  saveEdit(){

    this.postId=this.route.snapshot.paramMap.get('id');
    const posts=JSON.parse(localStorage.getItem('posts'));


    const title = this.editPostForm.get('title').value;
    const description = this.editPostForm.get('description').value;

    
    fetch('https://jsonplaceholder.typicode.com/posts/${postId}', {
    method: 'PATCH',
    body: JSON.stringify({
    title: this.editPostForm.get('title').value,
    description:this.editPostForm.get('description').value
    }),
    headers: {
    'Content-type': 'application/json; charset=UTF-8',
    },
    })
    .then((response) => response.json())
    .then((json) => {console.log(json)
      //console.log(posts[this.postId])
    //Object.assign(posts[this.postId],json);
    //console.log(posts[this.postId])
    this._router.navigateByUrl('/example');
    
    });



    

    }

    



}
