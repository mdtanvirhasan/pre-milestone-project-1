import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { UserComponent } from 'app/layout/common/user/user.component';
import { SharedModule } from 'app/shared/shared.module';
//import { profile } from 'console';
import { EditProfileComponent } from './edit-profile.component';
import { MatSelectModule } from '@angular/material/select';


@NgModule({
    declarations: [
        EditProfileComponent
    ],
    imports     : [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatDividerModule,
        MatIconModule,
        MatMenuModule,
        SharedModule,
        ReactiveFormsModule,
        MatSelectModule
    ],
    exports     : [
        EditProfileComponent,
        ReactiveFormsModule
    ]
})
export class EditProfileModule
{

}